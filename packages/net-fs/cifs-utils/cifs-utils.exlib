# Copyright 2011 Elias Pipping <pipping@exherbo.org>
# Copyright 2015 Thomas Witt
# Distributed under the terms of the GNU General Public License v2

# original configure script errors and we need to run sed on configure.ac
# to fix rst2man binary naming
# last checked: 6.8
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

require pam

export_exlib_phases src_prepare

SUMMARY="User-space tools required by the in-kernel CIFS filesystem"
HOMEPAGE="https://wiki.samba.org/index.php/LinuxCIFS_utils"
DOWNLOADS="https://download.samba.org/pub/linux-cifs/${PN}/${PNV}.tar.bz2"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS="
    ads [[ description = [ Enable Active Directory support ] ]]
    pam
    systemd
"

DEPENDENCIES="
    build:
       dev-python/docutils   [[ note = [ rst2man for man page ] ]]
    build+run:
       sys-apps/keyutils
       sys-libs/libcap
       ads? (
           dev-libs/talloc
           virtual/kerberos
       )
       pam? ( sys-libs/pam )
       systemd? ( sys-apps/systemd )
       !net-fs/samba[<3.6] [[
           description = [ File collision with Samba < 3.6 ]
           resolution = upgrade-blocked-before
       ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    ROOTSBINDIR=/usr/$(exhost --target)/bin
    --with-libcap
    --with-pamdir=$(getpam_mod_dir)
    --without-libcap-ng
    --enable-cifscreds
    --enable-man
    --disable-cifsacl
    --disable-cifsidmap
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'ads cifsupcall'
    pam
    systemd
)

cifs-utils_src_prepare() {
    #Fix naming of rst2man from our docutils
    edo sed -e "s/AC_CHECK_PROG(have_rst2man, rst2man/AC_CHECK_PROG(have_rst2man, rst2man.py/" -i ${WORK}/configure.ac
    edo sed -e "s/rst2man/rst2man.py/" -i ${WORK}/Makefile.am

    autotools_src_prepare
}

