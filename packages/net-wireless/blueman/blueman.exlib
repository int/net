# Copyright 2009 Jonathan Dahan <jedahan@gmail.com>
# Copyright 2011 Ali Polatel <alip@exherbo.org>
# Copyright 2014-2015 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'blueman-1.10.ebuild', which is:
#   Copyright 1999-2009 Gentoo Foundation

require option-renames [ renames=[ 'policykit polkit' ] ]
require github [ user='blueman-project' release="${PV/_/.}" suffix=tar.xz ]
require python [ blacklist='2' multibuild=false ]
require gsettings freedesktop-desktop gtk-icon-cache

export_exlib_phases src_prepare pkg_postinst pkg_postrm

SUMMARY="GTK+ Bluetooth Manager, designed to be simple and intuitive for everyday bluetooth tasks"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS="
    polkit
    thunar [[ description = [ Sendto plugin for Thunar ] ]]
    ( linguas: af am ar ast be bg bs ca cs cy da de el en_AU en_GB es et eu fa fi fr gl he hi hr hu
               id it ja kk ko lt lv mk mr ms nb nds nl pl pt pt_BR ro ru sk sl sq sr sv sw ta tr uk
               vi zh_CN zh_HK zh_TW )
"

# seems to run into issues with missing POTFILES
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-python/Cython[python_abis:*(-)?]
        dev-util/intltool[>=0.35.0]
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.32]
        dev-python/dbus-python[python_abis:*(-)?]
        gnome-bindings/pygobject:3[cairo][python_abis:*(-)?]
        net-wireless/bluez[>=4.61]
    run:
        net-wireless/obexd[>=0.47]
        virtual/notification-daemon
        x11-libs/gdk-pixbuf:2.0[gobject-introspection]
        x11-libs/gtk+:3[>=3.10.6][gobject-introspection]
        x11-libs/libnotify[gobject-introspection]
        x11-libs/pango[gobject-introspection]
        gnome-desktop/adwaita-icon-theme
        polkit? ( sys-auth/polkit:1 )
        thunar? ( xfce-base/Thunar )
    suggestion:
        media-sound/pulseaudio[>=0.9.15][bluetooth] [[
            description = [ Allow for using PulseAudio with Bluetooth audio devices ]
        ]]
        net-dns/dnsmasq [[
            description = [ Can be used to provide an internet connection to a Bluetooth device ]
        ]]
        virtual/dhcp [[
            description = [ Can be used to get/provide an internet connection to/from a Bluetooth device ]
        ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-settings-integration
    --disable-runtime-deps-check
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES+=(
    polkit
    'thunar thunar-sendto'
)

blueman_src_prepare() {
    default

    # fix pixmap path
    edo sed -e 's#@prefix@/share/pixmaps#/usr/share/pixmaps#g'  \
            -i blueman/Constants.py.in

    # fix shebangs
    edo sed -e "s:^#!/usr/bin/env python:#!/usr/$(exhost --target)/bin/python$(python_get_abi):"  \
            -i apps/*
}

blueman_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

blueman_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

