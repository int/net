# Copyright 2008, 2013 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'mime-support-3.39_p1.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

SUMMARY="MIME files 'mime.types' & 'mailcap', and support programs"
DESCRIPTION='
As these files can be used by all MIME compliant programs, they have been moved into their own package that others can depend upon.
Other packages add themselves as viewers/editors/composers/etc by using the provided "update-mime" program.
In addition, the commands "see", "edit", "compose", and "print" will display, alter, create, and print (respectively)
any file using a program determined from the entries in the mime.types and mailcap files.
'
HOMEPAGE="http://packages.debian.org/sid/${PN}"
DOWNLOADS="mirror://debian/pool/main/${PN:0:1}/${PN}/${PN}_${PV}.tar.gz"

LICENCES="public-domain"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-lang/perl:*
    run:
        sys-apps/file
"

WORK=${WORKBASE}/${PN}

src_prepare() {
    edo sed -e "s:/usr/bin/x-terminal-emulator:/usr/$(exhost --target)/bin/xterm:" \
            -i run-mailcap
}

src_install() {
    dobin run-mailcap
    newman run-mailcap.man run-mailcap.1

    for link in compose edit print see; do
        dosym run-mailcap /usr/$(exhost --target)/bin/${link}
        dosym run-mailcap.1 /usr/share/man/man1/${link}.1
    done

    insinto /etc
    doins mailcap mime.types
    newman mailcap.man mailcap.4
}

