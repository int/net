# Copyright 2016 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SCM_REPOSITORY="https://git.zx2c4.com/WireGuard"
SCM_TAG="${PV}"
require scm-git systemd-service

SUMMARY="Fast and secure kernelspace VPN"
DESCRIPTION="
WireGuard is an extremely simple yet fast and modern VPN that utilizes state-of-the-art
cryptography. It aims to be faster, simpler, leaner, and more useful than IPSec, while avoiding the
massive headache. It intends to be considerably more performant than OpenVPN. WireGuard is designed
as a general purpose VPN for running on embedded interfaces and super computers alike, fit for many
different circumstances. Initially released for the Linux kernel, it plans to be cross-platform and
widely deployable. It is currently under heavy development, but already it might be regarded as the
most secure, easiest to use, and simplest VPN solution in the industry.
"
HOMEPAGE="https://www.wireguard.com/"
DOWNLOADS=""

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS="examples"

DEPENDENCIES="
    build+run:
        net-libs/libmnl
        !net/wireguard-tools [[
            description = [ package renamed ]
            resolution = uninstall-blocked-before
        ]]

"

BUGS_TO="keruspe@exherbo.org philantrop@exherbo.org"

# Needs the kernel module and potentially messes up your network connection
RESTRICT="test"

MAKE_PARAMS=(
    BINDIR=/usr/$(exhost --target)/bin
    LIBDIR=/usr/$(exhost --target)/lib
    PLATFORM=linux
    RUNSTATEDIR=/run
    SYSTEMDUNITDIR=${SYSTEMDSYSTEMUNITDIR}
    V=1
    WITH_BASHCOMPLETION=yes
    WITH_WGQUICK=yes
    WITH_SYSTEMDUNITS=yes
)

DEFAULT_SRC_COMPILE_PARAMS=( "${MAKE_PARAMS[@]}" )
DEFAULT_SRC_INSTALL_PARAMS=( "${MAKE_PARAMS[@]}" )

src_compile() {
    edo cd src/tools
    default
}

src_install() {
    edo cd src/tools
    default
    edo cd ..
    emake DESTDIR="${IMAGE}" "${MAKE_PARAMS[@]}" dkms-install
    edo cd ..
    keepdir /etc/wireguard
    emagicdocs
    option examples && dodoc -r contrib/examples
}

