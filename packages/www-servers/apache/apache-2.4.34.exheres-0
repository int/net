# Copyright 2008, 2009 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

# Defaults: actions alias asis auth_basic cgi so userdir

require apache [ \
    apache_modules=[ \
        alias auth_digest authn_dbm authn_default authn_file authnz_ldap \
        authz_dbm authz_default authz_groupfile authz_host authz_owner \
        authz_user autoindex brotli bucketeer cache case_filter \
        case_filter_in cern_meta dav dav_fs dav_lock dbd deflate dir \
        disk_cache dumpio  echo env example expires ext_filter file_cache \
        filter headers http2 ident imagemap include info isapi ldap log_config \
        log_forensic logio md mem_cache mime mime_magic negotiation \
        optional_fn_export optional_fn_import optional_hook_export \
        optional_hook_import proxy proxy_ajp proxy_balancer proxy_connect \
        proxy_ftp proxy_http proxy_uwsgi rewrite setenvif speling ssl status \
        substitute suexec unique_id unixd usertrack version vhost_alias \
    ] \
]

PLATFORMS="~amd64 ~x86"

MYOPTIONS="
    apache_modules:
        authnz_ldap [[ requires = [ apache_modules: ldap ] ]]
        ( dav_fs dav_lock ) [[ requires = [ apache_modules: dav ] ]]
        ( proxy_ajp proxy_balancer proxy_connect proxy_ftp proxy_http
          proxy_uwsgi
        ) [[ requires = [ apache_modules: proxy ] ]]
"

DEPENDENCIES="
    build+run:
        apache_modules:brotli? ( app-arch/brotli[>=0.6.0] )
        apache_modules:http2? ( net-libs/nghttp2[>=1.15.0] )
        apache_modules:ldap? (
            dev-libs/apr-util:1[ldap]
            net-directory/openldap
        )
        apache_modules:md? (
            dev-libs/jansson
            net-misc/curl[>=7.50]
        )
        apache_modules:suexec? ( sys-libs/libcap )
    run:
        apache_modules:mime? ( app-admin/mime-support )
"

DEFAULT_SRC_PREPARE_PATCHES+=(
    -p0 "${FILES}"/${PN}-modules_ssl_ssl_private_h.patch
        "${FILES}"/${PN}-modules_ssl_ssl_engine_init_c.patch
)

