# Copyright 2013 Pierre Lejeune <superheron@gmail.com>
# Copyright 2014-2015 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

# tag: 1.2.0-beta1+android9 / rev: f5ff6e1bd819a347aa532c1ef1a9cf0c67387507
require github [ tag=f5ff6e1bd819a347aa532c1ef1a9cf0c67387507 ] cmake [ api=2 ]

SUMMARY="FreeRDP: A Remote Desktop Protocol implementation"
DESCRIPTION="
FreeRDP is a free implementation of the Remote Desktop Protocol (RDP), released under the Apache license.
"
HOMEPAGE+=" http://www.freerdp.com"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    alsa
    cups
    ffmpeg
    gstreamer
    pcsc [[ description = [ Support for smartcard authentification via pcsc-lite ] ]]
    pulseaudio
    server [[ description = [ Build the FreeRDP server ] ]]
    ffmpeg? ( ( providers: ffmpeg libav ) [[ number-selected = exactly-one ]] )
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        app-text/docbook-xsl-stylesheets
        dev-libs/libxslt
        virtual/pkg-config
    build+run:
        sys-libs/zlib
        x11-libs/libX11
        x11-libs/libXcursor
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libXi
        x11-libs/libXinerama
        x11-libs/libXrender
        x11-libs/libXv
        x11-libs/libxkbfile
        alsa? ( sys-sound/alsa-lib )
        cups? ( net-print/cups )
        ffmpeg? (
            providers:ffmpeg? ( media/ffmpeg )
            providers:libav? ( media/libav )
        )
        gstreamer? (
            media-libs/gstreamer:1.0
            media-plugins/gst-plugins-base:1.0
            x11-libs/libXrandr
        )
        pcsc? ( sys-apps/pcsc-lite )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        pulseaudio? ( media-sound/pulseaudio )
        server? (
            x11-libs/libXdamage
            x11-libs/libXrandr
        )
"

DEFAULT_SRC_PREPARE_PATCHES=(
     # Disable tests that try to open /dev/ttyS0
    "${FILES}"/FreeRDP-1.2.0_beta1_p20140912-Disable-serial-port-tests.patch
    "${FILES}"/1b663ceffe51008af7ae9749e5b7999b2f7d6698.patch
    "${FILES}"/bea27fd919b64ee8d97996409e279e1e83d13594.patch
    "${FILES}"/b7b66968f93f6ce75dd06d12638e14029bf3717b.patch
    -F3 "${FILES}"/555b7498b2abcd3809738f64417d2d7baa2fe359.patch
    "${FILES}"/368989526c32cdf9d680a397fede3cb773fa2609.patch
    "${FILES}"/${PN}-Fix-build-with-libressl-2.5.x.patch
    "${FILES}"/${PNV}-ffmpeg4.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DSTATIC_CHANNELS:BOOL=FALSE
    -DWITH_CHANNELS:BOOL=TRUE
    -DWITH_DIRECTFB:BOOL=FALSE
    -DWITH_GSM:BOOL=FALSE
    -DWITH_GSTREAMER_0_10:BOOL=FALSE
    -DWITH_IPP:BOOL=FALSE
    -DWITH_JPEG:BOOL=TRUE
    -DWITH_MANPAGES:BOOL=TRUE
    -DWITH_OPENH264:BOOL=FALSE
    -DWITH_OPENSLES:BOOL=FALSE
    -DWITH_X11:BOOL=TRUE
    -DWITH_XCURSOR:BOOL=TRUE
    -DWITH_XEXT:BOOL=TRUE
    -DWITH_XI:BOOL=TRUE
    -DWITH_XINERAMA:BOOL=TRUE
    -DWITH_XKBFILE:BOOL=TRUE
    -DWITH_XRENDER:BOOL=TRUE
    -DWITH_XV:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_WITHS=(
    'alsa ALSA'
    'cups CUPS'
    'ffmpeg FFMPEG'
    'gstreamer GSTREAMER_1_0'
    'pcsc PCSC'
    'pulseaudio PULSE'
    'server SERVER'
)

_ecmake_helper() {
    ecmake \
        "${CMAKE_SRC_CONFIGURE_PARAMS[@]}" \
        $(for s in "${CMAKE_SRC_CONFIGURE_OPTIONS[@]}" ; do
            cmake_option ${s}
        done ) \
        $(for s in "${CMAKE_SRC_CONFIGURE_OPTION_WITHS[@]}" ; do
            cmake_with ${s}
        done ) \
        "$@"
}

src_configure() {
    _ecmake_helper \
        -DBUILD_TESTING:BOOL=$(expecting_tests && echo TRUE || echo FALSE)
}

src_test() {
    esandbox allow_net "unix:${TEMP%/}/.pipe/*"
    default
    esandbox disallow_net "unix:${TEMP%/}/.pipe/*"
}

