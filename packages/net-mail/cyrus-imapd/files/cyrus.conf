# Standard standalone server configuration.

START {
  # Do not delete this entry!
  recover    cmd="ctl_cyrusdb -r"

  # This is only necessary if using idled for IMAP IDLE.
  #idled        cmd="idled"
}

# UNIX sockets start with a slash and are put into /var/imap/socket.
SERVICES {
  # Add or remove based on preferences.
  imap          cmd="imapd" listen="imap" prefork=0
  #pop3          cmd="pop3d" listen="pop3" prefork=0

  # Don't forget to generate the needed keys for SSL or TLS
  # (see doc/html/install-configure.html).
  #imaps         cmd="imapd -s" listen="imaps" prefork=0
  #pop3s        cmd="pop3d -s" listen="pop3s" prefork=0

  # Sieve support
  sieve         cmd="timsieved" listen="sieve" prefork=0

  # at least one LMTP is required for delivery
  #lmtp        cmd="lmtpd" listen="localhost:lmtp"
  lmtpunix      cmd="lmtpd" listen="/var/imap/socket/lmtp" prefork=0

  # this is only necessary if using notifications
   #notify      cmd="notifyd" listen="/var/imap/socket/notify" proto="udp" prefork=1
}

EVENTS {
  # This is required.
  checkpoint    cmd="ctl_cyrusdb -c" period=30

  # This is only necessary if using duplicate delivery suppression.
  delprune      cmd="cyr_expire -E 3" at=0400

  # This is only necessary if caching TLS sessions.
  tlsprune      cmd="tls_prune" at=0400

  # Index all user mailboxes (recommended)
  #squatter      cmd="squatter -r user" period=1440

  # The following two entries are provided as examples.

  # Process spam and ham folders for DSPAM training.
  #trainspam     cmd="/usr/bin/traindspam" period=60

  # Purge quarantine mailboxes. Delete email older than 14 days, 3am daily
  #purgespam     cmd="ipurge -f -d 14 user.*.quarantine" at=0300
}
