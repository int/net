# Copyright 2011 Wulf C. Krueger <philantrop@exherbo.org>
# Copyright 2013 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'squid-3.0.9.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ] \
    pam \
    systemd-service [ systemd_files=[ tools/systemd/${PN}.service ] ]

export_exlib_phases src_prepare src_configure src_test src_install pkg_postinst

PMAJOR=$(ever major)
PMINOR=$(ever range 2)
PPLEVL=$(ever range 3)

SUMMARY="Squid is a full-featured, high performance web proxy cache"
DESCRIPTION="
Squid is a fully-featured HTTP/1.0 proxy which is almost HTTP/1.1 compliant. Squid
offers a rich access control, authorization and logging environment to develop web
proxy and content serving applications.
"
HOMEPAGE="http://www.${PN}-cache.org"
DOWNLOADS="${HOMEPAGE}/Versions/v${PMAJOR}/${PNV}.tar.bz2"

UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/Versions/v${PMAJOR}/${PMAJOR}.${PMINOR}/RELEASENOTES.html"
UPSTREAM_CHANGELOG="${HOMEPAGE}/Versions/v${PMAJOR}/${PMAJOR}.${PMINOR}/changesets/SQUID_${PMAJOR}_${PMINOR}_${PPLEVL}.html"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/Doc [[ lang = en ]]"

LICENCES="GPL-2"
SLOT="0"
# Further possible auth backends: postgres, sqlite, samba
MYOPTIONS="
    caps [[ description = [ Support for Linux capabilities. ] ]]
    gnutls [[ description = [ GnuTLS is currently only used by the squidclient tool. ] ]]
    icap-client [[ description = [ ICAP is used to integrate with 3rd party products like malicious content scanners, URL filters, etc. ] ]]
    ldap [[ description = [ Support LDAP-based authentication. ] ]]
    mysql [[ description = [ Support MySQL-based authentication. ] ]]
    nis
    pam
    radius
    sasl
    snmp
    threads
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

# error: CommCalls.o: No such file or directory
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/perl:*
    build+run:
        group/squid
        user/squid
        dev-libs/expat
        dev-libs/libxml2:2.0
        dev-libs/nettle
        sys-libs/db:=[>=4&<5]
        caps? (
            net-libs/libnetfilter_conntrack
            sys-libs/libcap[>=2.19]
        )
        gnutls? ( dev-libs/gnutls[>=3.1.5] )
        ldap? ( net-directory/openldap )
        mysql? ( dev-db/mysql )
        pam? ( sys-libs/pam )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        sasl? ( net-libs/cyrus-sasl )
    test:
        dev-cpp/cppunit[>=1.12.1]
"

DEFAULT_SRC_INSTALL_EXTRA_DOCS=(
    QUICKSTART
    SPONSORS
)

squid_src_prepare() {
    # multiarch
    edo sed \
        -e "s:@EXHOST@:$(exhost --target):g" \
        "${FILES}"/${PN}-$(ever range 1-2)-defaults.patch \
        > "${WORKBASE}"/${PN}-$(ever range 1-2)-defaults.patch

    # multiarch and PID file
    edo sed \
        -e "s:/var/run:/run/squid:g"\
        -e "s:/usr/sbin:/usr/$(exhost --target)/bin:" \
        -e "s:/bin/kill:/usr/$(exhost --target)/bin/kill:" \
        -i tools/systemd/squid.service

    # workaround libtool error
    edo sed \
        -e 's:_LTDL_SETUP:LTDL_INIT([installable]):' \
        -i libltdl/configure.ac

    # use expatch for the above sed to work correctly
    expatch "${WORKBASE}"/${PN}-$(ever range 1-2)-defaults.patch

    autotools_src_prepare
}

squid_src_configure() {
    # not handled: MSNT-multi-domain, POP3, SMB, SMB_LM, SSPI
    local basic_auth_helpers="getpwnam,NCSA"
    option ldap   && basic_auth_helpers+=",LDAP"
    option mysql  && basic_auth_helpers+=",DB"
    option nis    && basic_auth_helpers+=",NIS"
    option pam    && basic_auth_helpers+=",PAM"
    option radius && basic_auth_helpers+=",RADIUS"
    option sasl   && basic_auth_helpers+=",SASL"

    # not handled: eDirectory
    local digest_auth_helpers="file"
    option ldap && digest_auth_helpers+=",LDAP"

    # not handled: AD_group, eDirectory_userip, kerberos_ldap_group, LM_group, SQL_session,
    #              time_quota, wbinfo_group
    local acl_helpers="file_userip,session,unix_group"
    option ldap && acl_helpers+=",LDAP_group"

    # not handled: SMB_LM, SSPI
    local ntlm_auth_helpers="fake"

    # not handled: SSPI. kerberos, wrapper
    local negotiate_auth_helpers="none"

    local diskio_modules="AIO,Blocking,DiskDaemon,DiskThreads,IpcIo,Mmapped"

    local storeio_modules="aufs,diskd,rock,ufs"

    local auth_helpers="basic,digest,negotiate,ntlm"

    local log_daemon_helpers="DB,file"

    local removal_policies="lru,heap"

    local security_cert_generators="file"

    local security_cert_validators="fake"

    local url_rewrite_helpers="fake"

    local myconf=(
        --datadir=/usr/share/squid
        --libexecdir=/usr/$(exhost --target)/libexec/squid
        --localstatedir=/var
        --sysconfdir=/etc/squid
        --enable-auth
        --enable-auth-basic="${basic_auth_helpers}"
        --enable-auth-digest="${digest_auth_helpers}"
        --enable-auth-negotiate"${negotiate_auth_helpers}"
        --enable-auth-ntlm="${ntlm_auth_helpers}"
        --enable-cheche-digests
        --enable-delay-pools
        --enable-disk-io="${diskio_modules}"
        --enable-external-acl-helpers="${acl_helpers}"
        --enable-esi
        --enable-eui
        --enable-follow-x-forwarded-for
        --enable-forw-via-db
        --enable-htcp
        --enable-icmp
        --enable-ipv6
        --enable-loadable-modules
        --enable-log-daemon-helpers="${log_daemon_helpers}"
        --enable-optimizations
        --enable-removal-policies="${removal_policies}"
        --enable-security-cert-generators="${security_cert_generators}"
        --enable-security-cert-validators="${security_cert_validators}"
        --enable-storeio="${storeio_modules}"
        --enable-url-rewrite-helpers="${url_rewrite_helpers}"
        --enable-wccp
        --enable-wccp2
        --disable-arch-native
        --disable-ecap
        --disable-kqueue
        --disable-ssl-crtd
        --disable-static
        --disable-strict-error-checking
        --disable-translation
        --with-default-user=squid
        --with-dl
        --with-filedescriptors=8192
        --with-large-files
        --with-logdir=/var/log/squid
        --with-nettle
        --with-openssl
        --with-pidfile=/run/squid/squid.pid
        --with-swapdir=/var/cache/squid
        --without-gnugss
        --without-heimdal-krb5
        --without-mit-krb5
        --without-nat-devpf
        $(expecting_tests --with-cppunit --without-cppunit)
        $(option_enable icap-client)
        $(option_enable snmp)
        $(option_with caps libcap)
        $(option_with caps netfilter-conntrack)
        $(option_with gnutls)
        $(option_with threads aio)
        $(option_with threads pthreads)
    )

    econf "${myconf[@]}"
}

squid_src_test() {
    # The header tests are partly broken due to missing includes. Figure them out
    # if you care. We can't do this in src_prepare because the bootstrap thingy
    # fails to work properly and eautoreconf breaks things.
    edo sed -i -e '/TESTS = /s:testHeaders::' src/Makefile

    default
}

squid_src_install() {
    default

    option pam && newpamd "${FILES}"/squid.pam squid

    # Needs to be suid root for looking into /etc/shadow
    local suid_auth_files=( /usr/$(exhost --target)/libexec/squid/basic_{ncsa,pam}_auth )

    for file in "${suid_auth_files[@]}"; do
        edo chown root:squid "${IMAGE}"/"${file}"
        edo chmod 4750 "${IMAGE}"/"${file}"
    done

    # ICMP pinger helper
    edo chown root:squid "${IMAGE}"/usr/$(exhost --target)/libexec/squid/pinger
    if option caps ; then
        edo chmod 0750 "${IMAGE}"/usr/$(exhost --target)/libexec/squid/pinger
    else
        edo chmod 4750 "${IMAGE}"/usr/$(exhost --target)/libexec/squid/pinger
    fi

    dodoc src/auth/basic/SASL/basic_sasl_auth.{conf,pam}

    install_systemd_files

    insinto /usr/$(exhost --target)/lib/tmpfiles.d
    hereins ${PN}.conf <<EOF
d /run/squid 0755 root root
EOF

    edo find "${IMAGE}" -type d -empty -delete

    keepdir /etc/ssl/squid

    diropts -m0755 -o squid -g squid
    keepdir /var/{cache,lib,log}/squid
}

squid_pkg_postinst() {
    # ICMP pinger helper
    if option caps ; then
        edo setcap cap_net_raw+ep /usr/$(exhost --target)/libexec/squid/pinger
    fi
}

