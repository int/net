# Copyright 2008 Daniel Mierswa <impulze@impulze.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service

SUMMARY="Automatic IPv6 Connectivity Client Utility"
DESCRIPTION="A client to configure an IPv6 tunnel to SixXS and other Tunnel Brokers"
HOMEPAGE="http://www.sixxs.net/tools/${PN}"
DOWNLOADS="http://www.sixxs.net/archive/sixxs/${PN}/unix/${PN}_${PV}.tar.gz"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-libs/gnutls
    run:
        sys-apps/iproute2
"

WORK="${WORKBASE}/${PN}"

DEFAULT_SRC_COMPILE_PARAMS+=(
    CC="${CC}"
)

src_prepare() {
    edo sed -e '/^CFLAGS=/s:.*::' \
            -i Makefile

    # let us do the stripping and don't override empty CFLAGS
    edo sed -e 's:^\sstrip $@::g' \
            -e '/^CFLAGS[[:space:]]*+=/s:-O3::' \
            -i unix-console/Makefile
}

src_install() {
    dosbin "unix-console/${PN}"
    insinto /etc
    doins "doc/${PN}.conf"
    dodoc doc/{README,HOWTO,changelog}
    doman doc/aiccu.1
    install_systemd_files
}

