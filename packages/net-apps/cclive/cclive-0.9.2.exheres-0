# Copyright 2009, 2012 Ingmar Vanhassel
# Copyright 2011 Sterling X. Winter <replica@exherbo.org>
# Copyright 2012,2013 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ suffix=tar.xz ]

SUMMARY="Command line video download tool"
DESCRIPTION="
cclive is a command line tool for downloading videos from YouTube and other similar video websites
that require Adobe Flash to view the video content. It has low memory footprint compared to other
similar tools.

Features:
 * Retry interrupted downloads automatically
 * quvi integration, supports 20+ websites
 * Flexible output filename options
 * Can go to background

Additional tools:
 * grake - YouTube link scanner
 * gcap - YouTube closed captions downloader
 * umph - YouTube feed parser for playlists, etc.
"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"

DEPENDENCIES="
    build:
        app-doc/asciidoc
        dev-util/pkg-config
    build+run:
        dev-libs/boost[>=1.42.0]
        dev-libs/libquvi[>=0.4.0]
        dev-libs/pcre[>=8.02]
        gnome-bindings/glibmm:2.4[>=2.24]
        net-misc/curl[>=7.18.0]
"

BUGS_TO="ingmar@exherbo.org"

