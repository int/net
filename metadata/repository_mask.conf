(
    dev-libs/tevent[~scm]
    media-video/rtmpdump[~scm]
    net-analyzer/fail2ban[~scm]
    net-fs/samba[~scm]
    net-im/bitlbee[~scm]
    net-im/bitlbee-steam[~scm]
    net-im/minbif[~scm]
    net-irc/weechat[~scm]
    net-irc/znc[~scm]
    net-libs/jreen[~scm]
    net-libs/libndp[~scm]
    net-libs/miniupnpc[~scm]
    net-misc/connman[~scm]
    net-misc/mosh[~scm]
    net-p2p/transmission[~scm]
    net-www/uzbl[~scm]
) [[
    *author = [ Exherbo developers ]
    *token = scm
    *description = [ Mask scm versions ]
]]

net-fs/cifs-utils[<=5.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 24 Apr 2012 ]
    token = security
    description = [ CVE-2012-1586 ]
]]

apache-mod/mod_wsgi[<4.1.0] [[
    author = [ Wulf C. Krueger <philantrop@exherbo.org> ]
    date = [ 14 Jun 2014 ]
    token = pending-removal
    description = [ Use >= 4.2.0. If you need 3.5 for some reason, yell. ]
]]

net-misc/tor[<0.2.2.39] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 20 Sep 2012 ]
    token = security
    description = [ Several security problems ]
]]

web-apps/cgit[<0.9.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 30 May 2013 ]
    token = security
    description = [ CVE-2013-2117 ]
]]

net-libs/libssh[<0.7.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 Feb 2016 ]
    token = security
    description = [ CVE-2016-0739 ]
]]

www-servers/nginx[<1.9.10] [[
    author = [ Friedrich Kröner <friedrich@mailstation.de> ]
    date = [ 1 Feb 2016 ]
    token = security
    description = [ CVE-2016-0742, CVE-2016-0746, CVE-2016-0747 ]
]]

net-fs/openafs[~scm] [[
    author = [ Dirk Heinrichs <dirk.heinrichs@altum.de> ]
    date = [ 11 May 2013 ]
    token = scm
    description = [ Masked SCM version ]
]]

(
    dev-libs/libquvi-scripts[>=0.9&<1.0]
    dev-libs/libquvi[>=0.9&<1.0]
    dev-libs/quvi[>=0.9&<1.0]
    net-apps/cclive[>=0.9&<1.0]
) [[
    *author = [ Johannes Nixdorf <mixi@exherbo.org> ]
    *date = [ 28 Jun 2013 ]
    *token = testing
    *description = [ This is a development release ]
]]

net-libs/libmicrohttpd[<0.9.32] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 21 Dec 2013 ]
    token = security
    description = [ CVE-2013-703{8,9} ]
]]

net-proxy/torsocks[~scm] [[
    author = [ Nicolas Braud-Santoni <nicolas+exherbo@braud-santoni.eu> ]
    date = [ 28 Nov 2013 ]
    token = scm
    description = [ This is a scm version for the rewrite of torsocks ]
]]

net-proxy/torsocks[<2] [[
    author = [ Nicolas Braud-Santoni <nicolas+exherbo@braud-santoni.eu> ]
    date = [ 19 Jan 2013 ]
    token = security
    description = [ See https://lists.torproject.org/pipermail/tor-dev/2013-June/004959.html ]
]]

net-remote/FreeRDP[<1.2.0_beta1_p20140612] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 24 Jul 2014 ]
    token = security
    description = [ CVE-2013-411{8,9} ]
]]

app-crypt/krb5[<1.16-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 02 Mar 2018 ]
    token = security
    description = [ CVE-2018-5729, CVE-2018-5730 ]
]]

net-proxy/squid[<4.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 27 Jul 2018 ]
    token = security
    description = [ CVE-2018-1172 ]
]]

net-misc/openvpn[<2.4.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 22 Jun 2017 ]
    token = security
    description = [ CVE-2017-7508, CVE-2017-7520 CVE-2017-7521, CVE-2017-7522 ]
]]

net-analyzer/tcpdump[<4.9.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 08 Sep 2017 ]
    token = security
    description = [ Multiple CVEs ]
]]

net-analyzer/wireshark[<2.6.3] [[
    author = [ Tom Briden <tom@decompile.me.uk> ]
    date = [ 13 Sep 2018 ]
    token = security
    description = [ CVE-2018-16056, CVE-2018-16057, CVE-2018-16058 ]
]]

net/net-snmp[<5.7.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 15 Jul 2015 ]
    token = security
    description = [ CVE-2014-3565 ]
]]

web-apps/cgit[<0.12] [[
    author = [ Kylie McClain <somasis@exherbo.org> ]
    date = [ 14 Jan 2015 ]
    token = security
    description = [ CVE-2016-1899, CVE-2016-1900, CVE-2016-1901 ]
]]

net-misc/socat[<1.7.3.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 01 Feb 2015 ]
    token = security
    description = [ http://www.dest-unreach.org/socat/contrib/socat-secadv7.html
                    http://www.dest-unreach.org/socat/contrib/socat-secadv8.html ]
]]

(
    dev-db/mariadb[<10.1.33]
    dev-db/mariadb[>=10.2&<10.2.15]
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 18 May 2018 ]
    *token = security
    *description = [ CVE-2018-{2755,2759,2761,2766,2771,2777,2781,2782,2784,
                               2786,2787,2810,2813,2817,2819} ]
]]

net-fs/samba[<4.8.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 15 Aug 2018 ]
    token = security
    description = [ CVE-2018-1{139,140,0858,0918,0919} ]
]]

net-mail/dovecot[<2.2.34] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 01 Mar 2018 ]
    token = security
    description = [ CVE-2017-{14461,15130} ]
]]

www-servers/apache[<2.4.30] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 16 Jul 2018 ]
    token = security
    description = [ CVE-2018-{1333,8011}  ]
]]

dev-scm/libgit2[<0.27.4] [[
    author = [ Rasmus Thomsen <cogitri@exherbo.org> ]
    date = [ 14 Aug 2018 ]
    token = security
    description = [ 0.27.4 fixed out-of-bounds reads when
processing smart-protocol "ng" packets. ]
]]

net-irc/weechat[<1.9.1] [[
    author = [ Johannes Nixdorf <mixi@exherbo.org> ]
    date = [ 30 Sep 2017 ]
    token = security
    description = [ CVE-2017-14727 ]
]]

media-video/rtmpdump[<2.4_p20151223] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 May 2017 ]
    token = security
    description = [ CVE-2015-827{0,1,2} ]
]]

net-mail/tnef[<1.4.15] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 29 Sep 2017 ]
    token = security
    description = [ CVE-2017-{6307,6308,6309,6310,8911} ]
]]

net-wireless/hostapd[<2.6] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 16 Oct 2017 ]
    token = security
    description = [ CVE-2017-130{77,78,79,80,81,82,86,87,88} ]
]]

net-remote/teamviewer[<13.2.13582] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 22 Aug 2018 ]
    token = security
    description = [ CVE-2018-143333 ]
]]

sys-auth/sssd[<1.16.1-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 Aug 2018 ]
    token = security
    description = [ CVE-2018-10852 ]
]]

net/mosquitto[<1.4.15] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 May 2018 ]
    token = security
    description = [ CVE-2017-7651, CVE-2017-7652 ]
]]

net-libs/nghttp2[<1.31.1] [[
    author = [ Heiko Becker <heireck@exherbo.org> ]
    date = [ 09 May 2018 ]
    token = security
    description = [ CVE-2018-1000168 ]
]]

dev-db/ldb[>=1.5.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 21 Sep 2018 ]
    token = testing
    description = [ Breaks Samba < 5.0 ]
]]

net-irc/znc[<1.7.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 18 Jul 2018 ]
    token = security
    description = [ CVE-2018-{14055,14056} ]
]]

net-p2p/transmission[<2.94] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 19 Jul 2018 ]
    token = security
    description = [ CVE-2018-5702 ]
]]
